<?php

	/*
		Template Name: Front Page
	*/
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
  <?php Starkers_Utilities::get_template_parts( array( 'parts/site-intro' ) ); ?>
        <!-- Site header wrap end -->
<section class="site-content" role="main">


  <?php Starkers_Utilities::get_template_parts( array( 'parts/gdd-009-b-contact-from','parts/shared/capabilities-module'/*, 'parts/resources-module'*/ ) ); ?>            

           
     <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/portfolio-slider','parts/industries-served-module' ) ); ?>
        </section>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
