<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */

/*
  Template Name: Resource Page
  */


?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
 <?php Starkers_Utilities::get_template_parts( array( 'parts/page-intro' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<!--Site Content-->
<section id="idContentTblCell"><!--PAGE START-->

           
<article class="site-content gdd-003-b">

<div class="resources-breadcrumb-wrap">
  <div class="inner-wrap">
  <ul class="resources-menu">
    <li><a href="#res-tab-1">Material Selection Guides</a></li>
    <li><a href="#res-tab-9">Spec Sheets</a></li>
    <li><a href="#res-tab-2">eBooks</a></li>
    <li><a href="#res-tab-3">Infographics</a></li>
    <li><a href="#res-tab-4">Videos</a></li>
    <li><a href="#res-tab-5">Why Work With Us</a></li>
    <li><a href="#res-tab-6">Industries</a></li>
    <li><a href="#res-tab-7">News</a></li>
    <li><a href="#res-tab-8">Partnerships</a></li>
  </ul>
  </div>
</div>
    
<div class="inner-wrap" id="res-wrap">

<section class="Resource-rfq">
  <div class="rr-wrap">
    <div class="rr-left">
      <p class="emph">Our Resource Library contains a variety of downloadable materials related to our company and products.</p>
    </div>
    <div class="rr-right">
      <a href="//metalstampings.americanindust.com/request-a-quote" class="gdd003-rfqcta">Request a Quote</a>
    </div>
  </div>
</section>

<section class="material-selection clearfix">
  
<div id="res-tab-1" class="res-smooth-scroll"></div>  

<h2><?php if(get_field('ms_header')): the_field('ms_header'); endif; ?></h2>  


<div class="rows-of-4 ms-wrap">

<?php if( have_rows('ms_items') ): while ( have_rows('ms_items') ) : the_row(); ?>

 <?php $ms_link = get_sub_field('ms_link');
                            if( $ms_link ): 
                                $link_url = $ms_link['url'];
                                $link_title = $ms_link['title'];
                            ?>
<a class="ms-item" href="<?php echo esc_url($link_url); ?>">

  <?php $ms_image = get_sub_field('ms_image');
             if( !empty($ms_image) ): ?>
<img  src="<?php echo $ms_image['url']; ?>" alt="<?php echo $ms_image['alt']; ?>" title="<?php echo $ms_image['alt']; ?>">
             
  <?php endif; ?>

<h4 class="ms-title"><?php echo esc_html($link_title); ?></h4>

</a> 

  <?php endif; ?>


    <?php endwhile;
            endif; ?>

</div>

</section>
<section class="material-selection spec-sheet clearfix">
  
<div id="res-tab-9" class="res-smooth-scroll"></div>  

<h2><?php if(get_field('ss_heading')): the_field('ss_heading'); endif; ?></h2>  


<div class="rows-of-4 ms-wrap">

<?php if( have_rows('ss_items') ): while ( have_rows('ss_items') ) : the_row(); ?>

 <?php $ss_link = get_sub_field('ss_link');
                            if( $ss_link ): 
                                $link_url = $ss_link['url'];
                                $link_title = $ss_link['title'];
                            ?>
<a class="ms-item" href="<?php echo esc_url($link_url); ?>">

  <?php $ss_image = get_sub_field('ss_image');
             if( !empty($ss_image) ): ?>
<img  src="<?php echo $ss_image['url']; ?>" alt="<?php echo $ss_image['alt']; ?>" title="<?php echo $ss_image['alt']; ?>">
             
  <?php endif; ?>

<h4 class="ms-title"><?php echo esc_html($link_title); ?></h4>

</a> 

  <?php endif; ?>


    <?php endwhile;
            endif; ?>

</div>

</section>


<section class="resources-ebook-module">
    
 <div id="res-tab-2" class="res-smooth-scroll"></div>  


<h2 class="rem-heading"><?php if(get_field('eb_title')): the_field('eb_title'); endif; ?></h2>  


<div class="owl-carousel rem-carousel owl-theme">

<?php if( have_rows('eb_items') ): $i=1; while ( have_rows('eb_items') ) : the_row(); ?>
 <div class="rem-item item">




 <?php $eb_link = get_sub_field('eb_link');
                            if( $eb_link ): 
                                $link_url = $eb_link['url'];
                                $link_title = $eb_link['title'];
                            ?>

<a class="rem-link" href="<?php echo esc_url($link_url); ?>">

 <?php $eb_image = get_sub_field('eb_image');
             if( !empty($eb_image) ): ?>
               <figure class="rem-img-wrap">
                <img class="rem-img" src="<?php echo $eb_image['url']; ?>" alt="<?php echo $eb_image['alt']; ?>" title="<?php echo $eb_image['alt']; ?>">
                </figure>
     <?php endif; ?>

 <h2 class="rem-title"><?php echo esc_html($link_title); ?></h2>

  </a>

  <?php endif; ?>


 


 </div>
   <?php $i++; endwhile;
            endif; ?>
</div>

</section>



<section class="infographics">
  
 <div id="res-tab-3" class="res-smooth-scroll"></div>  

<h2 class="info-heading"><?php if(get_field('info_title')): the_field('info_title'); endif; ?></h2>  


<div class="rows-of-3 infographics-wrap">


<?php if( have_rows('info_items') ): while ( have_rows('info_items') ) : the_row(); ?>

  <div class="<?php the_sub_field('info_classes');?>">

 <?php $info_link = get_sub_field('info_link');
                            if( $info_link ): 
                                $link_url = $info_link['url'];
                                $link_title = $info_link['title'];
          ?>

  <a href="<?php echo esc_url($link_url); ?>">

  <?php $info_image = get_sub_field('info_image');
             if( !empty($info_image) ): ?>
<img class="infographics-img" src="<?php echo $info_image['url']; ?>" alt="<?php echo $info_image['alt']; ?>" title="<?php echo $info_image['alt']; ?>">
             
  <?php endif; ?>

    <h2 class="infographics-title"><?php echo esc_html($link_title); ?></h2>

    </a>

  <?php endif; ?>

  </div>

  <?php $i++; endwhile;
      endif; ?>

</div>

</section>


<section class="video-content-module">

<div id="res-tab-4" class="res-smooth-scroll"></div> 
  <div class="inner-wrap">
   <div class="vc-wrap">
  <div class="vc-left">
    <h2 class="vc-title">
      <span>
 <?php $vid_link = get_field('vid_link');
                            if( $vid_link ): 
                                $link_url = $vid_link['url'];
                                $link_title = $vid_link['title'];
          ?>
        <a href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_title); ?></a>
      </span>
    <?php endif; ?>
      <?php the_field('vid_title');?>
     </h2>
  </div>

  <div class="vc-right">

  <?php $vid_image = get_field('vid_image');
  if( !empty($vid_image) ): ?>
<img class="vc-img" src="<?php echo $vid_image['url']; ?>" alt="<?php echo $vid_image['alt']; ?>" title="<?php echo $vid_image['alt']; ?>">       
  <?php endif; ?>

  </div>

   </div>

  </div>


</section>

<section class="why-work-section">

  <div id="res-tab-5" class="res-smooth-scroll"></div> 

  <div class="inner-wrap">

  <h2 class="ww-title">
    <?php the_field('wk_title');?>
<span>
   <?php $wk_link = get_field('wk_link');
                            if( $wk_link ): 
                                $link_url = $wk_link['url'];
                                $link_title = $wk_link['title'];
          ?>
<a href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_title); ?></a>
</span>
 <?php endif; ?>
</h2>

  </div>


</section>


<section class="industries-served-module">
  <div id="res-tab-6" class="res-smooth-scroll"></div> 

<h2 class="inds-heading"><?php if(get_field('is_title')): the_field('is_title'); endif; ?></h2>  
  <div class="inner-wrap">

<?php if( have_rows('is_items') ): $i=1; while ( have_rows('is_items') ) : the_row(); ?>
  <div class="inds-wrap">

<div class="inds-box">

 <?php $is_link = get_sub_field('is_link');
                            if( $is_link ): 
                                $link_url = $is_link['url'];
                                $link_title = $is_link['title'];
                            ?>
 <a class="inds-link" href="<?php echo esc_url($link_url); ?>"> 
 <?php $is_image = get_sub_field('is_image');
             if( !empty($is_image) ): ?>
               <figure>
                <img class="rem-img" src="<?php echo $is_image['url']; ?>" alt="<?php echo $is_image['alt']; ?>" title="<?php echo $is_image['alt']; ?>">
                </figure>
     <?php endif; ?>


<h3 class="inds-title"><?php echo esc_html($link_title); ?></h3>
</a>

 <?php endif; ?>

  </div>

    </div>
 <?php $i++; endwhile;
            endif; ?>

  </div>
</section>


<section class="resources-news-section">
<div id="res-tab-7" class="res-smooth-scroll"></div>  
<h2 class="news-heading">News from American Industrial</h2>

<div class="inner-wrap">
  <div class="news-wrap rows-of-3">
        <?php $counter = 3;
          $recentPosts = new WP_Query();
          $recentPosts->query('showposts=3');
        ?>
        <?php while ($recentPosts->have_posts()) : $recentPosts->the_post(); ?>
          <div class="news-posts">
            <h4 class="news-title">
              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h4>
            <div class="date-time"><time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time> <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?></div>
            <p class="news-text"><?php echo excerpt(25); ?></p>
            <a class="btn read-more-btn" href="<?php the_permalink(); ?>">Read More</a>
          
          </div>
        <?php endwhile; wp_reset_query(); ?>

</div>
<div class="blog-link"><a class="view-post-cta" href="https://www.americanindust.com/blog/">View All Posts</a></div>
</div>

</section>

<section class="injection-molders-module">

  <div id="res-tab-8" class="res-smooth-scroll"></div> 

  <div class="inner-wrap">

<div class="im-wrap">

  <div class="im-left">
    <p><?php the_field('pt_title');?></p>
  </div>

  <div class="im-right">
 <?php $pt_link = get_field('pt_link');
                            if( $pt_link ): 
                                $link_url = $pt_link['url'];
                                $link_title = $pt_link['title'];
          ?>
  <a class="im-cta" href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_title); ?></a>  
   <?php endif; ?>
  </div>

</div>

</div>

</section>



</div>



</article>  

       <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar','parts/shared/flexible-content-fullwidth' ) ); ?>

</div>
</section>

<?php endwhile; ?>



     
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>