<?php

	/*
		Template Name: Pillar Page
	*/
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?> 
</div>   
<?php Starkers_Utilities::get_template_parts( array( 'parts/page-intro' ) ); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
	<section class="additional-content pillar-page" role="main">
		<div class="inner-wrap">
		<article class="site-content-primary">
	       		<?php the_content(); ?>        
	        </article>
	    </div>
	    <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
		<?php Starkers_Utilities::get_template_parts( array( 'parts/page-bottom-destination' ) ); ?>

	</section>

<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox' ) ); ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>