<!-- Industries Served -->
<div class="gdd-industries-served-module gdd-011-b clearfix" <?php if (get_field('gism_background_image')): ?>style="display:none; background-image: url(<?php the_field('gism_background_image'); ?>);" <?php endif ?>>
  <div class="inner-wrap">
    <div class="accordion-tabs-wrap">
      <?php if(get_field('gism_heading')):?>
        <h2 class="gis-header"><?php the_field('gism_heading');?></h2>
      <?php endif;?>
      <ul class="accordion-tabs">
        <?php if( have_rows('gism_items') ): while ( have_rows('gism_items') ) : the_row(); ?>
          <li class="tab-header-and-content">
          
            <a href="javascript:void(0)" class="tab-link" rel="<?php the_sub_field('gism_background_image'); ?>"><span class="gis-content"><?php if(get_sub_field('gism_tab_title')) : ?><?php the_sub_field('gism_tab_title');?><?php endif;?></span></a>
            <div class="tab-content">
              <?php if(get_sub_field('gism_title')) : ?><h2 class="gis-title"><?php the_sub_field('gism_title');?></h2><?php endif;?>
              <?php if(get_sub_field('gism_text')) : ?><p class="gis-text"><?php the_sub_field('gism_text');?></p><?php endif;?>
              <?php if(get_sub_field('gism_cta_url')) : ?><a href="<?php the_sub_field('gism_cta_url');?>" class="btn gis-cta"><?php the_sub_field('gism_cta_text');?></a><?php endif;?>
            </div>
          </li>
        <?php endwhile;
        endif; ?>
      </ul>
    </div>
  </div>
</div>