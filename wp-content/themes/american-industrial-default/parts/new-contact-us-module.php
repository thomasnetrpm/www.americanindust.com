<!--Choose metal craft start-->
    <section class="choose-metal-craft-module">
      <div class="inner-wrap">
        <?php if(get_field('cmcm_heading')):?>
        <h2 class="cmcm-header"><?php the_field('cmcm_heading');?></h2>
        <?php endif;?>
        <?php if(get_field('cmcm_text')):?>
        <p class="cmcm-text"><?php the_field('cmcm_text');?></p>
        <?php endif;?>
        <div class="cmcm-bucket-wrap">
           <?php if( have_rows('cmcm_items') ): while ( have_rows('cmcm_items') ) : the_row(); ?>
          <?php 
                            $link = get_sub_field('im_link');
                            if( $link ): 
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                            ?>
          <div class="cmcm-item"><a href="<?php echo esc_url($link_url); ?>" class="cmcmi-link"><span class="cmcmi-image"><?php if(get_sub_field('cmcm_img')) : ?><?php $im_img = get_sub_field('cmcm_img'); ?><img src="<?php echo $im_img['url']; ?>" alt="<?php echo $im_img['title']; ?>" title="<?php echo $im_img['title']; ?>"><?php endif; ?></span><span class="cmcmi-title"><?php the_sub_field('cmcm_title');?></span></a>
          </div>
          <?php endif; ?> 
          <?php endwhile; endif; ?>
        </div>      
<?php if( have_rows('cm_item') ): ?>
<section class="home-capabilities-gdd-002-b" >
<div class="inner-wrap rows-of-3">
  <?php while ( have_rows('cm_item') ) : the_row(); ?>
<div>
<?php if(get_sub_field('cm_link')):?><a href="<?php the_sub_field('cm_link');?>" class="home-capabilities-item">
<?php if(get_sub_field('cm_image')):?><img class="hc-images" src="<?php the_sub_field('cm_image');?>" alt="<?php the_sub_field('cm_title');?>"><?php endif; ?>
<?php if(get_sub_field('cm_title')):?><h3 class="home-capabilities-item-header"><?php the_sub_field('cm_title');?></h3><?php endif; ?></a><?php endif; ?>
</div>
<?php endwhile; ?>
</div>
</section>
<?php endif; ?>
        <?php if(get_field('cmcm_bottom_content')):?>
        <p class="bottom-content">
          <?php the_field('cmcm_bottom_content');?>
        </p><?php endif; ?>
      </div>
    </section>
    <!--Choose metal craft End-->

