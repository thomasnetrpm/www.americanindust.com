<!--Infographics start-->
<section class="infographics-module">
  <div class="inner-wrap-fullwidth">
    <div class="im-content-wrap">
      <div class="im-content active" id="im-content-1">
        <span class="imc-number">1</span>
        <h2 class="imc-header">Engineer Design / Quoting</h2>
        <div class="imc-wrap">
          <div>
            <p>As a leader in design engineering services for tooling for over 30 years, at American Industrial Company, we utilize modern software and cutting edge testing tools to quickly and accurately provide complex prototypes, design assistance, reverse engineering, and the re-design of new or existing products</p>
            <p>Our Design Engineering services cover all the major areas of pre-production activities: from in-house die design, product and prototype development to material selection.</p>
          </div>
          <div>
            <p>Our team of Design Engineers is composed of AutoCAD with Diemaker, Solidworks with LogoPress and EdgeCAM experts, and is also highly proficient in a number of other equally advanced software tools.</p>
          </div>
        </div>
      </div>
      <div class="im-content" id="im-content-2">
        <span class="imc-number">2</span>
        <h2 class="imc-header">Tooling</h2>
        <div class="imc-wrap">
          <div>
            <p>At American Industrial Company, we operate a state-of-the-art, in-house tool-making facility that ensures we not only have control of tool making, but of the entire manufacturing process.</p>
<p>This facility is equipped to build dies from 6″ up to 85″ in length, all to tolerances of ±0.001″, and with our advanced knowledge of metallurgy, we can select the optimal die material. This includes various tool grade steels and pre hardened metals. This allows us to optimize costs by using die material that is matched to the run size and geometrical requirements.</p>
          </div>
          <div>
            <p>Die types include progressive, compound, secondary forming, and shallow draw dies, to dies used for coining, blanking, and piercing. If we can provide the <a href="https://www.americanindust.com/precision-metal-stamping-assembly-services.html">stamping process</a>, rest assured that the die came from our shop</p>
          </div>
        </div>
      </div>
      <div class="im-content" id="im-content-3">
        <span class="imc-number">3</span>
        <h2 class="imc-header">Metal Stamping Basics</h2>
        <div class="imc-wrap">
          <div>
            <p>Stamping — also called pressing — involves placing flat sheet metal, in either coil or blank form, into a stamping press. In the press, a tool and die surface form the metal into the desired shape. Punching, blanking, bending, coining, embossing, and flanging are all stamping techniques used to shape the metal.</p><p>Before the material can be formed, stamping professionals must design the tooling via CAD/CAM engineering technology. These designs must be as precise as possible to ensure each punch and bend maintains proper clearance and, therefore, optimal part quality. A single tool 3D model can contain hundreds of parts, so the design process is often quite complex and time-consuming.</p>
            <p>Once the tool’s design is established, a manufacturer can use a variety of machining, grinding, wire EDM and other manufacturing services to complete its production.</p>
          </div>
          <div>
            <p>A manufacturer might have to repeatedly change the tool on a single press or occupy a number of presses, each performing one action required for a completed part. Even using multiple presses, secondary machining services were often required to truly complete a part. For that reason, progressive die stamping is the ideal solution for metal parts with complex geometry to meet:</p>
            <ul>
              <li>Faster turnaround</li>
<li>Lower labor cost</li>
<li>Shorter run length</li>
<li>Higher repeatability</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="im-content" id="im-content-4">
        <span class="imc-number">4</span>
        <h2 class="imc-header">Progressive Die Stamping</h2>
        <div class="imc-wrap">
          <div>
            <p>Progressive die stamping features a number of stations, each with a unique function.</p>
<p>First, strip metal is fed through a progressive stamping press. Then, the strip unrolls steadily from a coil and into the die press, where each station in the tool then performs a different cut, punch, or bend. The actions of each successive station add onto the work of the previous stations, resulting in a completed part.</p>
          </div>
          <div></div>
        </div>
      </div>
      <div class="im-content" id="im-content-5">
        <span class="imc-number">5</span>
        <h2 class="imc-header">Quality Control / Assurance</h2>
        <div class="imc-wrap">
          <div><p>Our organization is based on a culture of quality, and it permeates every department, from the office to the dock.</p>
            <p>We utilize quality management systems that employ a number of very effective tools. This includes statistical process control, or “SPC” which is a method of using statistics to define and measure product quality and the production parts approval process. This is often used in the automotive industry for defining complex quality standards.</p></div>
          <div><p>Our quality department also employs a number of advanced software tools such as GAGETrak and are also equipped with various cutting edge inspection systems such as our Microvu optical camera system.</p></div>
        </div>
      </div>
    </div>
    <div class="im-svg-wrap">
      <div class="imsvgw-svg">
        <img src="<?php bloginfo('template_url'); ?>/img/metal-stamping-services-img.svg">
      </div>
      <div class="imsvg-dot">
        <a  href="#im-content-1" class="imsvgd-dot imsvgd-dot1 yellow"><span>Step1</span></a>
        <a  href="#im-content-2" class="imsvgd-dot imsvgd-dot2 yellow"><span>Step2</span></a>
        <a  href="#im-content-3" class="imsvgd-dot imsvgd-dot3 yellow"><span>Step3</span></a>
        <a  href="#im-content-4" class="imsvgd-dot imsvgd-dot4 yellow"><span>Step4</span></a>
        <a  href="#im-content-5" class="imsvgd-dot imsvgd-dot5 yellow"><span>Step5</span></a>
      </div>
    </div>
  </div>
</section>
<!--Infographics end-->