<?php if( have_rows('cm_item') ): ?>
<section class="home-capabilities-gdd-002-b" >
<div class="inner-wrap rows-of-3">
  <?php while ( have_rows('cm_item') ) : the_row(); ?>
<div>
<?php if(get_sub_field('cm_link')):?><a href="<?php the_sub_field('cm_link');?>" class="home-capabilities-item">
<?php if(get_sub_field('cm_image')):?><img class="hc-images" src="<?php the_sub_field('cm_image');?>" alt="<?php the_sub_field('cm_title');?>"><?php endif; ?>
<?php if(get_sub_field('cm_title')):?><h3 class="home-capabilities-item-header"><?php the_sub_field('cm_title');?></h3><?php endif; ?></a><?php endif; ?>
</div>
<?php endwhile; ?>
</div>
</section>
<?php endif; ?>