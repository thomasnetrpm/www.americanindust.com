<!--Secondary Content-->
<aside class="site-content-secondary">

	<!-- Aside CTAs -->
	<?php if(get_field('aside_cta') ): ?>
	<div class="cta-aside">
	<?php the_field('aside_cta'); ?>
	</div>                 
	<?php elseif(get_field('global_aside_cta','option') ): ?>
	<div class="cta-aside">
	<?php the_field('global_aside_cta','option'); ?>
	</div>
	<?php endif; ?>

	<!-- Page Conditionals -->
	<?php if ('951' == $post->post_parent) : ?>
	   <h3>Other Capabilities</h3>
       <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Capabilities',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>
       
	<?php elseif ('972' == $post->ID) : ?>
			<h3>Portfolio Projects</h3>
       <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Portfolio',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>

  <?php elseif ('1205' == $post->post_parent) : ?>
			<h3>Press Room</h3>
       <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Press Room',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>

  <?php elseif ('956' == $post->post_parent) : ?>
			<h3>Precision Metal Stamping</h3>
       <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Metal Stamping',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>

  <?php elseif ('974' == $post->ID ||'974' == $post->post_parent) : ?>

			<h3>Stainless Steel Portfolios</h3>
       <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Stainless Steel',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>

  <?php elseif ('976' == $post->ID || '976' == $post->post_parent) : ?>
			<h3>Carbon Steel Portfolios</h3>
       <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Carbon Steel',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>

  <?php elseif ('978' == $post->ID || '978' == $post->post_parent) : ?>
			<h3>Non Ferrous Portfolios</h3>
       <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Non Ferrous',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>

  <?php elseif ('978' == $post->ID || '978' == $post->post_parent) : ?>
      <h3>Non Ferrous Portfolios</h3>
       <?php 
    // $postid = get_the_ID();
     wp_nav_menu(array(
            'menu'            => 'Non Ferrous',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
      //'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>


 <?php elseif ('1556' == $post->ID): ?>
      <h3>Industries</h3>
       <?php 
    // $postid = get_the_ID();
     wp_nav_menu(array(
            'menu'            => 'Industries',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
      
        )); ?>
	<?php endif; ?>

	<!-- Additional Aside Content -->
	<?php if(get_field('additional_aside_content') ): ?>
		<?php the_field('additional_aside_content'); ?>
	</div>                 
	<?php elseif(get_field('global_additional_aside_content','option') ): ?>
		<?php the_field('additional_additional_aside_content'); ?>
	<?php endif; ?>

</aside>


