<?php Starkers_Utilities::get_template_parts( array( 'parts/resources-module') ); ?>
<!--Site Footer-->
<!--Site Footer-->
<footer class="site-footer dark-module gdd-001-a-site-footer" role="contentinfo" style="display: none;">
  <div class="inner-wrap">
    <p class="footer-address"><span class="text-large-header">American Industrial Company</span> 1080 Tri-State Parkway, Gurnee, IL 60031
      <br> Phone: 847.855.9200 Fax: 847.855.9300
      <br> Email: <a href="https://mail.google.com/mail/?view=cm&amp;fs=1&amp;tf=1&amp;to=AIC@americanindust.com" target="_blank">AIC@americanindust.com</a>
      <br>
      <iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" src="https://platform.twitter.com/widgets/follow_button.10495f50328f320357081ee891e346f2.en.html#_=1424780984842&amp;dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;screen_name=metal_stampings&amp;show_count=false&amp;show_screen_name=false&amp;size=m" class="twitter-follow-button twitter-follow-button" title="Twitter Follow Button" data-twttr-rendered="true" style="width: 59px; height: 20px;"></iframe>
      <script type="text/javascript">
      !
      function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (!d.getElementById(id)) {
          js = d.createElement(s);
          js.id = id;
          js.src = "//platform.twitter.com/widgets.js";
          fjs.parentNode.insertBefore(js, fjs);
        }
      }(document, "script", "twitter-wjs");
      </script>
      <br>
      <img src="<?php bloginfo('template_url'); ?>/img/industrynet-logo.jpg" alt="American Industrial Company" class="industrynet-logo">
    </p>
    <p class="footer-associations"><span class="text-large-header">Proud Members of:</span> <span class="col-3-mobile"> <a rel="nofollow" href="//www.pma.org" target="_blank"><img alt="Members of Precision Metalforming Association" src="<?php bloginfo('template_url'); ?>/img/logo-pma.jpg"></a> <a rel="nofollow" href="//www.ima-net.org/" target="_blank"><img alt="Members of Illinois Manufacturing Association" src="<?php bloginfo('template_url'); ?>/img/logo-ima.jpg"></a> <a rel="nofollow" href="//www.tmanet.com/" target="_blank"><img alt="Members of Tooling &amp; Manufacturing Association" src="<?php bloginfo('template_url'); ?>/img/logo-tma.jpg"></a> </span></p>
    <p class="small-footer">Site by <a rel="nofollow" href="//business.thomasnet.com/rpm" target="_blank">Results Powered Marketing</a> | <a href="https://www.americanindust.com/about-us/" rel="nofollow">About Us</a> | <a href="<?php bloginfo('url'); ?>/privacy.html" rel="nofollow">Privacy Policy</a> | <a href="<?php bloginfo('url'); ?>/terms-of-service.html" rel="nofollow">Terms Of Service</a> | <a href="//metalstampings.americanindust.com/contact-us-american-industrial-company" rel="nofollow">Contact Us</a> | <a href="<?php bloginfo('url'); ?>/sitemap.html">Sitemap</a></p>
  </div>
</footer>
<footer class="site-footer dark-module gdd-001-b-site-footer" role="contentinfo" >
  <div class="inner-wrap">
    <div class="sf-left-wrap">
      <div class="sf-address">
        <p class="footer-address"><span class="text-large-header">American Industrial Co.</span> 1080 Tri-State Parkway, Gurnee, IL 60031
          <br> Phone: 847.855.9200 Fax: 847.855.9300
          <br> Email: <a href="https://mail.google.com/mail/?view=cm&amp;fs=1&amp;tf=1&amp;to=AIC@americanindust.com" target="_blank">AIC@americanindust.com</a>
          <br>
        </p>
        <p class="sf-industry"><img src="<?php bloginfo('template_url'); ?>/img/industrynet-logo.jpg" alt="American Industrial Company" class="industrynet-logo"></p>
      </div>
      <span class="sf-link-wrap">
     <a href="/terms-of-service.html" class="sf-link">Terms and Conditions</a> <a href="/blog" class="sf-link">Blog</a> <a href="//metalstampings.americanindust.com/lp-iso-certification" class="sf-link">ISO</a> <a href="https://www.americanindust.com/about-us/" class="sf-link">About</a> <a href="/value-added-secondary-services" class="sf-link">Secondary Services</a>
     </span>
    </div>
    <div class="footer-associations"><span class="text-member">Proud Members of:</span> <span class="col-3-mobile"> <a rel="nofollow" href="//www.pma.org" target="_blank"><img alt="Members of Precision Metalforming Association" src="<?php bloginfo('template_url'); ?>/img/logo-pma.jpg"></a> <a rel="nofollow" href="//www.ima-net.org/" target="_blank"><img alt="Members of Illinois Manufacturing Association" src="<?php bloginfo('template_url'); ?>/img/logo-ima.jpg"></a> <a rel="nofollow" href="//www.tmanet.com/" target="_blank"><img alt="Members of Tooling &amp; Manufacturing Association" src="<?php bloginfo('template_url'); ?>/img/logo-tma.jpg"></a> </span>
      <p class="sf-follow-us"><span>Follow Us:</span> <a href="https://www.facebook.com/AmericanIndustrialCo" target="_blank"><img alt="Facebook" src="<?php bloginfo('template_url'); ?>/img/ico-facebook.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-facebook.png">
                            </a>
        <a href="https://twitter.com/metal_stampings" target="_blank"><img alt="Twitter" src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-twitter.png">
                            </a>
        <a href="https://plus.google.com/106144561124140792375/?rel=author" target="_blank"><img alt="Google Plus" src="<?php bloginfo('template_url'); ?>/img/ico-googleplus.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-googleplus.png">
                            </a>
        <a href="https://www.linkedin.com/company/american-industrial-co-" target="_blank"><img alt="LinkedIn" src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-linkedin.png">
                            </a>
      </p>
    </div>
    <p class="small-footer">Site by <a rel="nofollow" href="//business.thomasnet.com/rpm" target="_blank" class="tms">Thomas Marketing Services</a> | <a href="<?php bloginfo('url'); ?>/privacy.html" rel="nofollow">Privacy Policy</a> | <a href="<?php bloginfo('url'); ?>/sitemap.html">Sitemap</a></p>
  </div>
</footer>
<!--<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js?ver=1.10.2'></script>
        <script src="<?php bloginfo('template_url'); ?>/js/production.min.js"></script>-->
