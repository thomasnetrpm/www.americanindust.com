<!--Site Header-->
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/search-module') ); ?>
<div class="site-header-wrap">
  
 

  <header class="site-header gdd-001-b-site-header gdd-005-b" role="banner" >

    <div class="sh-sticky-wrap">

 <div class="top-badge">   
<!-- Thomas Supplier Badge -->
<a href="https://www.thomasnet.com/profile/01285763?src=tnbadge" target="_blank" class="tn-badge__link">
<img
src="https://img.thomascdn.com/badges/shield-tier-v-sm.png?cid=01285763"
srcset="https://img.thomascdn.com/badges/shield-tier-v-sm-2x.png?cid=01285763 2x"
alt="Thomas Supplier" class="tn-badge__img" /></a>
<!-- End Thomas Supplier Badge -->

<a href="https://www.iqsdirectory.com/laser-cutting/" target="_blank"><img style="margin:5px 0;" border="0" src="https://www.iqsdirectory.com/images/badge/iqs-trusted-partner.png" width="120px" height="76px" alt="IQS Directory Trusted Partner" title="IQS Directory Trusted Partner"/><span style="color:transparent; position:absolute; z-index:-1; text-indent:-9999px">IQS Directory Trusted Partner</span></a>
</div>








    <div class="inner-wrap">
      <a href="<?php bloginfo('url'); ?>" class="logo"><img src="<?php bloginfo('template_url'); ?>/img/gdd-001-site-logo.png" data-png-fallback="<?php bloginfo('template_url'); ?>/img/american-industrial-logo.png" alt=""></a>
      <div class="utility-nav">
        <nav><a class="un-phone" href="tel:847-855-9200"><span>Tel.</span> 847.855.9200</a>
           <a href="/blog">Blog</a><a href="/resources">Resources</a><a href="//metalstampings.americanindust.com/lp-iso-certification">ISO</a> <a href="/portfolio/">Portfolios</a> <a href="/about-american-industrial-company/">About</a> <a href="//metalstampings.americanindust.com/contact-us">Contact</a></nav>
        <span class="sh-icons">
          <a href="//metalstampings.americanindust.com/lp-request-a-quote" class="btn rfq gdd-001-cta gdd-005-b-cta">Request for Quote</a>
            <a href="#search" class="sh-ico-search search-link"><span>Search</span></a>
            <a href="#menu" class="sh-ico-menu menu-link"><span>Menu</span></a>
        </span>
      </div>
      <div class="site-nav-container">
        <div class="snc-header">
          <a href="" class="close-menu menu-link">Close</a>
        </div>
        <?php wp_nav_menu(array(
            'menu'            => 'GDD-005-B Primary Nav',
            'container'       => 'nav',
            'container_class' => 'site-nav',
            'menu_class'      => 'sn-level-1',
            'walker'        => new themeslug_walker_nav_menu
        )); ?>
        <a href="#menu" class="sh-ico-search search-link"><span></span></a>
        <a href="//metalstampings.americanindust.com/lp-request-a-quote" class="btn rfq gdd-001-cta">Request a Quote</a>
        <div class="snc-footer">
          <?php get_search_form(); ?>
        </div>
      </div>
      <a href="" class="site-nav-container-screen menu-link">&nbsp;</a>
      <!--Site Nav END-->
    </div>
  </div>
    <!--Site Nav-->
  </header>
