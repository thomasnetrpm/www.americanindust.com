<section class="home-capabilities">
            <div class="<?php if (is_page( '1205' )) : ?>rows-of-2<?php else: ?>inner-wrap rows-of-3<?php endif; ?>">
             <a href="<?php bloginfo('url'); ?>/resources" class="home-capabilities-item">
                    <img src="<?php bloginfo('template_url'); ?>/img/resources-2.png" alt="Resources">
                    <h3 class="home-capabilities-item-header">Resources</h3>
                    <span class="home-capabilities-item-body col-2-mobile">
                        <ul class="bucket_content">
               <li>Browse our eBooks to learn about precision metal stamping, choosing the right materials, and more. </li>
            </ul>
                    </span>
               </a>
               <a href="//metalstampings.americanindust.com/blog" class="home-capabilities-item">
                    <img src="<?php bloginfo('template_url'); ?>/img/news-2.png" alt="American Industrial Blog">
                    <h3 class="home-capabilities-item-header">News</h3>
                    <span class="home-capabilities-item-body col-2-mobile">
                        <ul>
                           <li>Stay current with industry news from our blog and read interesting facts about Metal Stamping.</li>
                        </ul>
                    </span>
               </a>
               <a href="<?php bloginfo('url'); ?>/industries-served.html" class="home-capabilities-item">
                    <img src="<?php bloginfo('template_url'); ?>/img/industries-2.png" alt="Industries served">
                    <h3 class="home-capabilities-item-header">Industries</h3>
                    <span class="home-capabilities-item-body col-2-mobile">
                       <ul>
               <li>Discover which industries we serve and find out how an American Industrial partnership can work for you. </li>
            </ul>
                    </span>
               </a>
               <a href="<?php bloginfo('url'); ?>/videos" class="home-capabilities-item">
                    <img src="<?php bloginfo('template_url'); ?>/img/videos-2.png" alt="American Industrial Videos">
                    <h3 class="home-capabilities-item-header">Videos</h3>
                    <span class="home-capabilities-item-body col-2-mobile">
                        <ul>
                           <li>Watch how our machines work and what leads to American Industrial's high Quality Assurance standards.</li>
                        </ul>
                       
                    </span>
               </a>
             
 </div>
        </section>               