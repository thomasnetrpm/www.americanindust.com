<section class="grey-module"><div class="inner-wrap">
      <h2 class="wsubtext" style="text-align: center;"><?php
$parent_id = wp_get_post_parent_id( $post_ID );
 if ( $parent_id == '951' ) : ?>Related Portfolios<?php elseif( $parent_id == '972' ): ?>Other Portfolios<?php elseif(is_home()): ?>At American Industrial we provide precision manufacturing services to customers in a wide range of industries.<?php endif; ?> Gallery</h2>
 <?php if(is_home()): ?>
 <p>Here are just a few examples of the innovative solutions and value added products and services that we deliver every day.</p><?php endif; ?> 
      <div class="flexslider product-carousel">
                        <ul class="slides">
                            <li>
                                <a href="<?php bloginfo('url'); ?>/fabricated-channel-power-tool-industry.html" class="product-carousel-item">
                                    <span class="product-carousel-img"><img src="<?php bloginfo('template_url'); ?>/img/thumb-fabricated-channel.png" alt="Fabricated Channel for the Power Tool Industry"></span>
                                    <h3 class="product-carousel-item-header">Fabricated Channel<br> for the Power Tool Industry</h3>
                                    
                                </a>
                             </li> 
                            <li>
                                <a href="<?php bloginfo('url'); ?>/fabrication-upper-yoke-power-tool-industry.html" class="product-carousel-item">
                                    <span class="product-carousel-img"><img src="<?php bloginfo('template_url'); ?>/img/thumb-fabrication-of-upper-yoke.png" alt="Fabrication of Upper Yoke for the Power Tool Industry"></span>
                                    <h3 class="product-carousel-item-header">Fabrication of Upper Yoke<br> for the Power Tool industry</h3>
                                    
                                </a>
                            </li> 
                            <li>
                                <a href="<?php bloginfo('url'); ?>/fabrication-jaw-assembly-tooling-industry.html" class="product-carousel-item">
                                    <span class="product-carousel-img"><img src="<?php bloginfo('template_url'); ?>/img/thumb-fabrication-of-jaw-assembly.png" alt="Fabrication of Jaw Assembly for the Tooling Industry "></span>
                                    <h3 class="product-carousel-item-header">Fabrication of Jaw Assembly<br> for the Tooling Industry</h3>
                                    
                                </a>
                             </li> 
                            <li>
                                <a href="<?php bloginfo('url'); ?>/fabrication-brass-inserts-plastic-industry.html" class="product-carousel-item">
                                    <span class="product-carousel-img"><img src="<?php bloginfo('template_url'); ?>/img/thumb-fabrication-of-brass-inserts.png" alt="Fabrication of Brass Inserts for the Plastic Industry"></span>
                                    <h3 class="product-carousel-item-header">Fabrication of Brass Inserts<br> for the Plastic Industry</h3>
                                    
                                </a>
                            </li> 
                            <li>
                                <a href="<?php bloginfo('url'); ?>/fabrication-pneumatic-power-tool-industry.html" class="product-carousel-item">
                                    <span class="product-carousel-img"><img src="<?php bloginfo('template_url'); ?>/img/thumb-pneumatic-tool.png" alt="Fabrication of Pneumatic for the Power Tool Industry"></span>
                                    <h3 class="product-carousel-item-header">Fabrication of Pneumatic<br> for the Power Tool Industry</h3>
                                    
                                </a>
                             </li> 
                            <li>
                                <a href="<?php bloginfo('url'); ?>/fabrication-probe-power-tool-industry.html" class="product-carousel-item">
                                    <span class="product-carousel-img"><img src="<?php bloginfo('template_url'); ?>/img/thumb-fabrication-of-upper-probe.png" alt="Fabrication of Probe for the Power Tool Industry"></span>
                                    <h3 class="product-carousel-item-header">Fabrication of Probe<br> for the Power Tool Industry</h3>
                                    
                                </a>
                             </li> 
                            <li>
                                <a href="<?php bloginfo('url'); ?>/fabrication-muffler-cover-power-tool-industry.html" class="product-carousel-item">
                                    <span class="product-carousel-img"><img src="<?php bloginfo('template_url'); ?>/img/thumb-fabrication-of-muffler-cover.png" alt="Fabrication of Muffler Cover for the Power Tool Industry"></span>
                                    <h3 class="product-carousel-item-header">Fabrication of Muffler Cover<br> for the Power Tool Industry</h3>
                                    
                                </a>
                             </li> 
                            <li>
                                <a href="<?php bloginfo('url'); ?>/fabrication-plate-power-tool-industry.html" class="product-carousel-item">
                                    <span class="product-carousel-img"><img src="<?php bloginfo('template_url'); ?>/img/thumb-fabrication-of-plates.png" alt="Fabrication of Plate for the Power Tool Industry"></span>
                                    <h3 class="product-carousel-item-header">Fabrication of Plate<br> for the Power Tool Industry</h3>
                                    
                                </a>
                            </li>  
                            <li>
                                <a href="<?php bloginfo('url'); ?>/assembly-latch-power-tool-industry.html" class="product-carousel-item">
                                    <span class="product-carousel-img"><img src="<?php bloginfo('template_url'); ?>/img/thumb-assembly-of-latches.png" alt="Assembly of Latch for the Power Tool Industry"></span>
                                    <h3 class="product-carousel-item-header">Assembly of Latch<br> for the Power Tool Industry</h3>
                                    
                                </a>
                           </li> 
                            <li>
                                <a href="<?php bloginfo('url'); ?>/assembly-base-power-tool-industry.html" class="product-carousel-item">
                                    <span class="product-carousel-img"><img src="<?php bloginfo('template_url'); ?>/img/thumb-assembly-of-bases.png" alt="Assembly of Base for the Power Industry"></span>
                                    <h3 class="product-carousel-item-header">Assembly of Base<br> for the Power Industry"</h3>
                                    
                                </a>
                            </li>                            
                        </ul>
                    </div>
                    </div>
</section>