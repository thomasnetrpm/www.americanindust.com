 
        <header class="site-intro gdd-001-b-site-intro">
            <div class="hi-wrap"><a href="https://www.americanindust.com/blog/aic-takes-preventative-measures-against-covid-19/">
            <div class="hiw-left"><p>We are taking proactive steps against COVID-19 to ensure the safety and security of our employees and vendors</p></div><span class="btn">Read more</span></a></div>
            <div class="inner-wrap">
                <?php if(get_field('si_header')):?><h1 class="si-header"><?php the_field('si_header');?></h1><?php endif;?>
                <?php if(get_field('si_text')):?><p class="si-text"><?php the_field('si_text');?></p><?php endif;?>

                <div class="si-ctas">
                   <?php if(get_field('si_cta_text')): ?><a href="<?php the_field('si_cta_link'); ?>" class="btn si-cta gdd-001-cta"><?php the_field('si_cta_text'); ?></a><?php endif;?>
                </div>

            </div>
        </header>
        <div class="si-video gdd-007-b">
          <video autoplay="autoplay" loop="loop" muted="muted" playsinline="playsinline" width="100%" height="100%">
            <source src="https://www.americanindust.com/assets/americanIndustrial-hero-video.webm" type="video/webm">
            <source src="https://www.americanindust.com/assets/americanIndustrial-hero-video.mp4" type="video/mp4">
            </video>
          </div>
        </div>
        <!-- Site header wrap end -->