<section class="gdd-004-b-featured-ebooks">
         <div class="inner-wrap">
            <div class="fe-content">
               <h2 class="fec-heading">Looking for additional resources?</h2>
               <p class="fec-text">Our Resource Library contains a variety of downloadable materials related to our company and products.</p>
               <span class="gdd004-library-btn"><a href="https://www.americanindust.com/resources/">Browse Our Library</a></span>
                <span class="gdd004-contact-btn"><a href="https://metalstampings.americanindust.com/lp-contact-us-american-industrial-company">Contact Us</a></span>
            </div>
         </div>
  </section>