<?php if (is_page( '1206' )) : ?>
<section class="site-content gdd-003-b" style="display: none;">
<div class="resources-breadcrumb-wrap">
	<div class="inner-wrap">
	<ul class="resources-menu">
		<li><a href="#res-tab-1">Material Selection Guides</a></li>
		<li><a href="#res-tab-2">eBooks</a></li>
		<li><a href="#res-tab-3">Infographics</a></li>
		<li><a href="#res-tab-4">Videos</a></li>
		<li><a href="#res-tab-5">Why Work With Us</a></li>
		<li><a href="#res-tab-6">Industries</a></li>
		<li><a href="#res-tab-7">News</a></li>
		<li><a href="#res-tab-8">Partnerships</a></li>
	</ul>
	</div>
</div>
		
<div class="inner-wrap" id="res-wrap">

<section class="Resource-rfq">
	<div class="rr-wrap">
		<div class="rr-left">
			<p class="emph">Our Resource Library contains a variety of downloadable materials related to our company and products.</p>
		</div>
		<div class="rr-right">
			<a href="//metalstampings.americanindust.com/request-a-quote" class="gdd003-rfqcta">Request a Quote</a>
		</div>
	</div>
</section>

<section class="material-selection clearfix">
	
<div id="res-tab-1" class="res-smooth-scroll"></div>	
<h2>Material Selection Guides</h2>	
<div class="rows-of-4 ms-wrap">

<a class="ms-item" href="https://metalstampings.americanindust.com/lp-material-selection-guide-aluminum?hsCtaTracking=e571988b-8f0e-497a-8fe2-3b8263b0df7a%7C1e726b59-ed55-4e1a-b80a-849e45077551" target="_blank"><img alt="" src="http://www.americanindust.com/assets/aluminium-selection.png">
<h4 class="ms-title">Aluminum Selection Guide</h4>
</a> 

<a class="ms-item" href="https://metalstampings.americanindust.com/lp-material-selection-guide-cold-rolled-steel-american-industrial-company" target="_blank"><img alt="" src="http://www.americanindust.com/assets/coldrolled-selection.png">
<h4 class="ms-title">Cold Rolled Steel Selection Guide</h4>
</a> 

<a class="ms-item" href="https://metalstampings.americanindust.com/lp-material-selection-guide-stainless-steel-american-industrial-company" target="_blank"><img alt="" src="http://www.americanindust.com/assets/stainless-selection.png">
<h4 class="ms-title">Stainless Steel Selection Guide</h4>
</a> 

<a class="ms-item" href="https://metalstampings.americanindust.com/lp-stainless-steel-finishes-ebook-download" target="_blank"><img alt="" src="http://www.americanindust.com/assets/stainless-finishes.png">
<h4 class="ms-title">Stainless Steel Finishes</h4>
</a> 

 
</div>

</section>


<section class="resources-ebook-module">
    
 <div id="res-tab-2" class="res-smooth-scroll"></div>   
<h2 class="rem-heading">eBooks</h2>

<div class="owl-carousel rem-carousel owl-theme">

 <div class="rem-item item">
<a class="rem-link" href="https://metalstampings.americanindust.com/lp-metal-stamped-concerns-power-tools">
<figure class="rem-img-wrap"><img class="rem-img" src="http://www.americanindust.com/assets/power-tool-ind.png" alt="metal stamp components"></figure>
 <h2 class="rem-title">Metal Stamped Component Concerns in the Power Tool Industry</h2>
  </a>
 </div>

 <div class="rem-item item">
<a class="rem-link" href="https://metalstampings.americanindust.com/lp-download-metal-stamping-vendor-ebook">
<figure class="rem-img-wrap"><img class="rem-img" src="http://www.americanindust.com/assets/metal-stamping-vendor.png" alt="Metal Stamping Vendor"></figure>
 <h2 class="rem-title">What to Look for in a Metal Stamping Vendor</h2>
 </a>
 </div>


 <div class="rem-item item">
<a class="rem-link" href="https://metalstampings.americanindust.com/lp-energy-industry-metal-stamped-components">
<figure class="rem-img-wrap"><img class="rem-img" src="http://www.americanindust.com/assets/energy-industry-app.png" alt="Energy Industry Applications"></figure>
 <h2 class="rem-title">Component Concerns for Energy Industry Applications</h2>
 </a>
 </div>




<div class="rem-item item">
<a class="rem-link" href="https://cta-redirect.hubspot.com/cta/redirect/304398/3c77f817-6805-49c6-9432-b41f4314696a?__hstc=268190417.1f937a59d1d76782d9bd561b7d3f2c34.1562063008551.1562063008551.1562063008551.1&__hssc=268190417.1.1562063008551&__hsfp=1021165237">
<figure class="rem-img-wrap"><img class="rem-img" src="http://www.americanindust.com/assets/prototype.png" alt="Prototype"></figure>
 <h2 class="rem-title">Design to Prototype to Production</h2>
 </a>
 </div>

<div class="rem-item item">
<a class="rem-link" href="https://metalstampings.americanindust.com/lp-hot-cold-rolled-steel-infographic">
<figure class="rem-img-wrap"><img class="rem-img" src="http://www.americanindust.com/assets/AI-hot-cold-rolled-steel-new.png" alt="Hot Rolled"></figure>
 <h2 class="rem-title">Hot Rolled Steel Vs. Cold Rolled Steel Infographic</h2>
 </a>
 </div>



<div class="rem-item item">
<a class="rem-link" href="https://metalstampings.americanindust.com/lp-carbon-stainless-nonferrous-metals">
<figure class="rem-img-wrap"><img class="rem-img" src="http://www.americanindust.com/assets/Strengths-of-Steel-3d-1-new.png" alt="Key Strengths"></figure>
 <h2 class="rem-title">Key Strengths & Applications of Carbon Steel, Stainless Steel, and Non-ferrous Metals </h2>
 </a>
 </div>


<div class="rem-item item">
<a class="rem-link" href="https://metalstampings.americanindust.com/lp-metal-stamping-2018-trend-report">
<figure class="rem-img-wrap"><img class="rem-img" src="http://www.americanindust.com/assets/Metal-Stamping-3D-1-new.png" alt="Trend Report"></figure>
 <h2 class="rem-title">Metal Stamping 2018 Trend Report</h2>
 </a>
 </div>

 <div class="rem-item item">
<a class="rem-link" href="https://metalstampings.americanindust.com/lp-made-in-america-aic-in-house-capabilities">
<figure class="rem-img-wrap"><img class="rem-img" src="http://www.americanindust.com/assets/aic-inhouse-capabilities-thumb-new.png" alt="AIC"></figure>
 <h2 class="rem-title">Made in America: AIC’s In-House Capabilities</h2>
 </a>
 </div>

 <div class="rem-item item">
<a class="rem-link" href="https://cta-redirect.hubspot.com/cta/redirect/304398/a1b24500-1ea7-43a4-bef7-5d8c17ed0ea7?__hstc=268190417.1f937a59d1d76782d9bd561b7d3f2c34.1562063008551.1562063008551.1562063008551.1&__hssc=268190417.1.1562063008551&__hsfp=1021165237">
<figure class="rem-img-wrap"><img class="rem-img" src="http://www.americanindust.com/assets/trends-thumb-2017-new.png" alt="Trend Report 2017"></figure>
 <h2 class="rem-title">Metal Stamping Trends Report 2017</h2>
 </a>
 </div>


 <div class="rem-item item">
<a class="rem-link" href="https://metalstampings.americanindust.com/lp-5-reasons-you-should-be-utilizing-precision-laser-cutting-ebook">
<figure class="rem-img-wrap"><img class="rem-img" src="http://www.americanindust.com/assets/5-Reasons-Why-You-Should-Be-Utilising.png" alt="5 Reasons precision laser cutting"></figure>
 <h2 class="rem-title">5 Reasons you should be utilizing Precision Laser Cutting ebook</h2>
 </a>
 </div>



</div>
</section>



<section class="infographics">
	
 <div id="res-tab-3" class="res-smooth-scroll"></div> 	
<h2 class="info-heading">Infographics</h2>

<div class="rows-of-2 infographics-wrap">
	<div class="infographics-left">
		<a href="https://metalstampings.americanindust.com/lp-learn-the-factors-for-selecting-a-metal-stamper">
		<img class="infographics-img" src="http://www.americanindust.com/assets/AIC_-_9_Factors_to_Consider_When_Choosing_a_Metal_Stamper_-_Guide.jpg">
		<h2 class="infographics-title">The Metal Stamper Must-Haves</h2>
	  </a>
	</div>
	<div class="infographics-right">
		<a href="https://metalstampings.americanindust.com/lp-automotive-metal-stamping">
		<img class="infographics-img" src="http://www.americanindust.com/assets/American_Industrial_-_Automotive_Infographic.jpg">
		<h2 class="infographics-title">Cars & Motorcycles: How We Help</h2>
	</a>
	</div>
</div>

</section>


<section class="video-content-module">
<div id="res-tab-4" class="res-smooth-scroll"></div> 	
	<div class="inner-wrap">
   <div class="vc-wrap">
	<div class="vc-left">
		<h2 class="vc-title"><span><a href="https://www.americanindust.com/videos/">Click here</a></span> to view our full collection of video content.</h2>
	</div>
	<div class="vc-right">
		<img class="vc-img" src="http://www.americanindust.com/assets/vc-image.png">
	</div>
   </div>
	</div>
</section>

<section class="why-work-section">
	<div id="res-tab-5" class="res-smooth-scroll"></div> 
	<div class="inner-wrap">
		<h2 class="ww-title">Why work with American Industrial?<span><a href="https://www.americanindust.com/about-american-industrial-company.html">Find out here!</a></span></h2>
	</div>
</section>


<section class="industries-served-module">
	<div id="res-tab-6" class="res-smooth-scroll"></div> 
	<h2 class="inds-heading">Industries Served</h2>
	<div class="inner-wrap">
		<div class="inds-wrap">
			<div class="inds-box">
<a class="inds-link" href="https://www.americanindust.com/electrical-metal-stamping/">
<figure><img src="http://www.americanindust.com/assets/inds-electric.jpg" alt="Electric" title="Electric"></figure>
<h3 class="inds-title">electric</h3>
</a>
			</div>
			<div class="inds-box">
				<a class="inds-link" href="https://www.americanindust.com/metal-stamping-for-construction">
<figure><img src="http://www.americanindust.com/assets/inds-construction.jpg" title="construction" alt="construction"></figure>
<h3 class="inds-title">construction</h3>
</a>
			</div>
			<div class="inds-box">
				<a class="inds-link" href="https://www.americanindust.com/hardware/">
<figure><img src="http://www.americanindust.com/assets/inds-hardware.jpg" title="hardware" alt="hardware"></figure>
<h3 class="inds-title">hardware</h3>
</a>
			</div>
			<div class="inds-box">
				<a class="inds-link" href="https://www.americanindust.com/fastening/">
<figure><img src="http://www.americanindust.com/assets/inds-fastening.jpg" title="fastening" alt="fastening"></figure>
<h3 class="inds-title">fastening</h3>
</a>
			</div>
			<div class="inds-box">
				<a class="inds-link" href="https://www.americanindust.com/appliance/">
<figure><img src="http://www.americanindust.com/assets/inds-appliances.jpg" title="appliance" alt="appliance"></figure>
<h3 class="inds-title">appliance</h3>
</a>
			</div>
		</div>
	</div>
</section>


<section class="resources-news-section">
<div id="res-tab-7" class="res-smooth-scroll"></div>	
<h2 class="news-heading">News from American Industrial</h2>

<div class="inner-wrap">
  <div class="news-wrap rows-of-3">
        <?php $counter = 3;
          $recentPosts = new WP_Query();
          $recentPosts->query('showposts=3');
        ?>
        <?php while ($recentPosts->have_posts()) : $recentPosts->the_post(); ?>
          <div class="news-posts">
            <h4 class="news-title">
              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h4>
            <div class="date-time"><time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time> <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?></div>
            <p class="news-text"><?php echo excerpt(25); ?></p>
            <a class="btn read-more-btn" href="<?php the_permalink(); ?>">Read More</a>
          
          </div>
        <?php endwhile; wp_reset_query(); ?>

</div>
<div class="blog-link"><a class="view-post-cta" href="https://www.americanindust.com/blog/">View All Posts</a></div>
</div>

</section>

<section class="injection-molders-module">
	<div id="res-tab-8" class="res-smooth-scroll"></div>	
	<div class="inner-wrap">
<div class="im-wrap">
	<div class="im-left">
		<p>Do your clients have projects that need both rubber
and metal elements?</p>
	</div>
	<div class="im-right">
		<a class="im-cta" href="https://metalstampings.americanindust.com/lp-sign-up-for-our-injection-molders-partnership-program">Sign up for Our Injection Molders Partnership Program</a>
	</div>
</div>
</div>
</section>



</div>


</section>
<?php endif; ?>