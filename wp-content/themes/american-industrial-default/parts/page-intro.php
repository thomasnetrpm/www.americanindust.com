<header class="page-header"><div class="inner-wrap"><h1 class="page-header-h1"><?php if(is_home()):  ?>AMERICAN INDUSTRIAL COMPANY BLOG
               <?php elseif(is_search()): ?>Search Results for '<?php echo get_search_query(); ?>'
                <?php elseif(is_404()): ?>404: Page not found <?php elseif(is_archive()): ?>Archives
           <?php elseif(get_field('alternative_h1')): the_field('alternative_h1'); else: the_title(); endif; ?></h1></div>
</header>
</div>