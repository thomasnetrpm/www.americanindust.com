<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
 <?php Starkers_Utilities::get_template_parts( array( 'parts/page-intro' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<!--Site Content-->
<section id="idContentTblCell"><!--PAGE START-->

    
<!--Resource page Varb-->
    <?php Starkers_Utilities::get_template_parts( array('parts/resources-module-varb') ); ?>
          
  
      <div class="inner-wrap" id="main-inner">

      <?php if(get_field('custom_featured_img') ): ?>
<div class="featured-img">
<?php the_field('custom_featured_img'); ?>
</div>
<?php elseif(has_post_thumbnail() ): ?>
<div class="featured-img">
<?php the_post_thumbnail(); ?>
</div>
<?php endif; ?>


<article class="site-content-primary">
<?php the_content(); ?> 
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
<?php if (is_page( '9' )) : ?>
<!--Sitemap Page-->
   <ul>
   <?php
   // Add pages you'd like to exclude in the exclude here
   wp_list_pages(
   array(
   'exclude' => '',
   'title_li' => '',
   )
   );
   ?>
   </ul>
<?php endif; ?>    
<?php if (is_page( '951' )) : ?>
<!--Capabilities module-->
    <?php Starkers_Utilities::get_template_parts( array('parts/shared/capabilities-module') ); ?>
        <?php endif; ?>   
        <?php if (is_page( '1205' )) : ?>
<!--Capabilities module-->
    <?php Starkers_Utilities::get_template_parts( array('parts/shared/pressroom-module') ); ?>
        <?php endif; ?>  

              <?php if (is_page( '1556' )) : ?>
<!--Capabilities module-->
    <?php Starkers_Utilities::get_template_parts( array('parts/new-contact-us-module') ); ?>

        <?php endif; ?>  





       </article>        
       <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar','parts/shared/flexible-content-fullwidth' ) ); ?>
</div>
</section>

<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox' ) ); ?>

<?php
$parent_id = wp_get_post_parent_id( $post_ID );
 if ( ( $parent_id == '951' ) || ($parent_id == '972') ) : 
 Starkers_Utilities::get_template_parts( array( 'parts/shared/portfolio-slider' ) ); 
endif; ?>      
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>