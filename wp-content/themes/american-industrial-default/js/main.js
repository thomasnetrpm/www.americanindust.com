//PNG Fallback

if (!Modernizr.svg) {
    var images = $('img[data-png-fallback]');
    images.each(function(i) {
        $(this).attr('src', $(this).data('png-fallback'));
    });
}

//Lightbox


/*$('.lightbox-infographic,.lightbox').magnificPopup({
        type: 'image',
        removalDelay: 500, //Delaying the removal in order to fit in the animation of the popup
        mainClass: 'mfp-fade', //The actual animation
       overflowY: 'hidden',
    autoFocusLast:false,
    fixedContentPos: true,
        callbacks: {
          close: function() {
        $('html').removeClass('mfg-popup-open');
          $.each(this.items, function( index, value ) {
            if (value.el) {
              $(value.el[0]).addClass('tse-remove-border');
            } else {
              $(value).removeClass('tse-remove-border');
            }
          });
        },
        open: function() {
          var self = this;
          $('.input-group-append.search-close').trigger('çlick');
          if ($( window ).height() < $( document ).height()) {
             $('html').addClass('mfg-popup-open');
          }
          self.wrap.on('click.pinhandler', 'img', function() {
            self.wrap.toggleClass('mfp-force-scrollbars');
          });
        },
        beforeClose: function() {
          this.wrap.off('click.pinhandler');
          this.wrap.removeClass('mfp-force-scrollbars');
        }
    },
     
    image: {
    verticalFit: false
  }
    });*/

$(document).ready(function() {
  $('.lightbox-infographic,.lightbox').magnificPopup({
    type: 'image',
    removalDelay: 500, //Delaying the removal in order to fit in the animation of the popup
    mainClass: 'mfp-fade', //The actual animation
    overflowY: 'hidden',
    fixedContentPos: true,
    fixedBgPos: true,
    callbacks: {
    close: function() {
      $.each(this.items, function( index, value ) {
        if (value.el) {
          $(value.el[0]).addClass('tse-remove-border');
        } else {
          $(value).removeClass('tse-remove-border');
        }
      });
    },
    },
  });
});


$(document).ready(function() {
 $('.large-lightbox').magnificPopup({
   type: 'image',
   removalDelay: 500, //Delaying the removal in order to fit in the animation of the popup
   mainClass: 'mfp-fade', //The actual animation
   overflowY: 'scroll',
   fixedContentPos: true,
   fixedBgPos: true,
   enabled: true,
   callbacks: {
    open: function() {
     var self = this;
     self.wrap.on('click.pinhandler', 'img', function() {
       self.wrap.toggleClass('mfp-force-scrollbars');
     });
   },
   beforeClose: function() {
         this.wrap.off('click.pinhandler');
     this.wrap.removeClass('mfp-force-scrollbars');
   },
   close: function() {
     $.each(this.items, function( index, value ) {
       if (value.el) {
         $(value.el[0]).addClass('tse-remove-border');
       } else {
         $(value).removeClass('tse-remove-border');
       }
     });
   },
   },
   image: {
           verticalFit: false
       }
 });
});


$(document).ready(function() {
  $('.popup-youtube, .popup-vimeo, .popup-gmaps , .popup-mp4').magnificPopup({
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 500,
      preloader: false,
      overflowY: 'hidden',
      fixedContentPos: true,
      fixedBgPos: true,
      callbacks: {
        open: function(){
          $('body').addClass('mfp-zoom-out-cur ');
        },
      close: function() {
        $('body').removeClass('mfp-zoom-out-cur');
        $('body').addClass('no-transition');
        setTimeout(function () { 
          $('body').removeClass('no-transition');
        }, 2000);
        /$(this.ev).addClass('tse-remove-border');/
        $.each(this.items, function( index, value ) {
          if (value.el) {
            $(value.el[0]).addClass('tse-remove-border');
          } else {
            $(value).removeClass('tse-remove-border');
          }
        });
      },
    },
      iframe: {
        patterns: {
          youtube: {
            index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).
            id: 'v=', // String that splits URL in a two parts, second part should be %id%
            // Or null - full URL will be returned
            // Or a function that should return %id%, for example:
            // id: function(url) { return 'parsed id'; }

            src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0' // URL that will be set as a source for iframe.
          }
        },
        srcAction: 'iframe_src', // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
      }
    });
});

$(document).ready(function() {
    $('.lightbox-form').magnificPopup({
        type: 'iframe',
        removalDelay: 500, //Delaying the removal in order to fit in the animation of the popup
        mainClass: 'mfp-fade', //The actual animation
    });
});

//Toggle Boxes
$(document).ready(function() {
    $('body').addClass('js');
    var $activatelink = $('.activate-link');

    $activatelink.click(function() {
        var $this = $(this);
        $this.toggleClass('active').next('div').toggleClass('active');
        return false;
    });

});

/*//Responsive Navigation
$(document).ready(function() {
    $('body').addClass('js');
    var $menu = $('.site-nav-container'),
        $menulink = $('.menu-link'),
        $menuTrigger = $('.menu-item-has-children > a'),
        $searchLink = $('.search-link'),
        $siteSearch = $('.search-module'),
        $siteWrap = $('.site-wrap');

    $searchLink.click(function(e) {
        e.preventDefault();
        $searchLink.toggleClass('active');
        $siteSearch.toggleClass('active');
    });

    $menulink.click(function(e) {
        e.preventDefault();
        $menulink.toggleClass('active');
        $menu.toggleClass('active');
        $siteWrap.toggleClass('nav-active');
    });

    $menuTrigger.click(function(e) {
        //e.preventDefault();
        var $this = $(this);
        $this.toggleClass('active').next('ul').toggleClass('active');
    });

});*/


//Responsive Navigation
$(document).ready(function() {
    $('body').addClass('js');
    var $menu = $('.site-nav-container'),
        $menulink = $('.menu-link'),
        $menuTrigger = $('.menu-item-has-children > a'),
        $searchLink = $('.search-link'),
        $siteSearch = $('.search-module'),
        $siteWrap = $('.site-wrap');

    $searchLink.click(function(e) {
        e.preventDefault();
        $searchLink.toggleClass('active');
        $siteSearch.toggleClass('active');
        $('#search-site').focus();
    });

    $menulink.click(function(e) {
        e.preventDefault();
        $menulink.toggleClass('active');
        $menu.toggleClass('active');
        $siteWrap.toggleClass('nav-active');
    });

    $('.menu-item-has-children').append("<span class='m-subnav-arrow'></span>");

    $('.m-subnav-arrow').click(function() {
        $(this).toggleClass('active');
        var $this = $(this).prev(".sn-level-2");
        $this.toggleClass('active').next('ul').toggleClass('active');
    });

});





//Show More
$(document).ready(function() {
    $(".showmore").after("<p><a href='#' class='show-more-link'>More</a></p>");
    var $showmorelink = $('.showmore-link');
    $showmorelink.click(function() {
        var $this = $(this);
        var $showmorecontent = $('.showmore');
        $this.toggleClass('active');
        $showmorecontent.toggleClass('active');
        return false;
    });
});

//Delayed Popup with localstorage to show popup only once
// $(document).ready(function() {
//   if (!localStorage.getItem('popup_show')) {

//     setTimeout(function() {
//       $.magnificPopup.open({
//         items: {
//           src: '#delayed-popup' //ID of inline element
//         },
//         type: 'inline',
//         removalDelay: 500, //Delaying the removal in order to fit in the animation of the popup
//         mainClass: 'mfp-fade', //The actual animation

//       });
//     }, 5000); //Initial popup delay, 5 seconds

//     localStorage.setItem('popup_show', 'true'); // Set the flag in localStorage
//   }
// });

//Delayed Popup with localstorage to show popup only once
$(document).ready(function() {
  var findPopupId = $('#delayed-popup').length; // if #delayed-popup exists, findPopupId = 1;
  if (findPopupId > 0) { // only run when #delayed-popup exists
    var wWidth = $(window).width(); // set variable of window width
    if (wWidth >= 320) { //only trigger on tablet or larger to prevent mobile private browsers who don't allow cookies (safari)
      if (localStorage.getItem('popup_show') === null && localStorage.getItem('exitintent_show') === null ) { // check if key is present in local storage to prevent re-triggering
        setTimeout(function() {
          window.$.magnificPopup.open({
            items: {
              src: '#delayed-popup' //ID of inline element
            },
            type: 'inline',
            removalDelay: 500, // delaying the removal in order to fit in the animation of the popup
            mainClass: 'mfp-fade mfp-fade-side', // The actual animation
          });
          localStorage.setItem('popup_show', 'true'); // set the key in local storage
        }, 1000); // delay in millliseconds until the modal triggers
        setTimeout(function() {
                window.$.magnificPopup.close();
        },30000);
      }
    }
  }
});

//Show More
$(document).ready(function() {



    var $expandlink = $('.headexpand');
    $expandlink.click(function() {
        var $this = $(this);
        var $showmorecontent = $('.showmore');


        $this.toggleClass('active').next().toggleClass('active');
        $showmorecontent.toggleClass('active');
        return false;
    });

    $('.request-quote > a > span').addClass('gdd-001-cta');
    $('.si-ctas .hs-cta-img').addClass('gdd-001-cta');

      
});

//Smooth Scroll - Detects a #hash on-page link and will smooth scroll to that position. Will not affect regular links.
$(function() {
  $('.smooth-scroll').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});


$(document).ready(function(){

  // hide #back-top first
  $(".back-top").hide();
  
  // fade in #back-top
  $(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        $('.back-top').fadeIn();
      } else {
        $('.back-top').fadeOut();
      }
    });

    // scroll body to 0px on click
    $('.back-top a').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });
  });

});

//Flexslider 
/*$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false
  });
});
*/


//Flexslider 
(function() {

    // store the slider in a local variable
    var $window = $(window),
        flexslider;

    // tiny helper function to add breakpoints
    function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
            (window.innerWidth < 900) ? 3 : 4;
    }

    /* $(function() {
       SyntaxHighlighter.all();
     });*/

    $window.load(function() {
        $('.flexslider').flexslider({
            animation: "slide",
            animationLoop: false,
            controlNav: false,
            itemWidth: 230,
            itemMargin: 25,
            minItems: getGridSize(), // use function to pull in initial value
            maxItems: getGridSize() // use function to pull in initial value
        });
    });

    // check grid size on resize event
    $window.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
    });
}());


//Sticky Nav
/*$(function() {
 
    // grab the initial top offset of the navigation 
    var sticky_navigation_offset_top = $('.nav-wrap').offset().top;
     
    // our function that decides weather the navigation bar should have "fixed" css position or not.
    var sticky_navigation = function(){
        var scroll_top = $(window).scrollTop(); // our current vertical position from the top
         
        // if we've scrolled more than the navigation, change its position to fixed to stick to top,
        // otherwise change it back to relative
        if (scroll_top > sticky_navigation_offset_top) { 
            $('.nav-wrap').addClass('nav-sticky');
        } else {
            $('.nav-wrap').removeClass('nav-sticky'); 
        }   
    };
     
    // run our function on load
    sticky_navigation();
     
    // and run it again every time you scroll
    $(window).scroll(function() {
         sticky_navigation();
    });
 
});



//Slide in CTA
$(function() {
  var slidebox = $('#slidebox');
  if (slidebox) {
    $(window).scroll(function(){
        var distanceTop = $('#last').offset().top - $(window).height();
        if  ($(window).scrollTop() > distanceTop)
            slidebox.animate({'right':'0px'},300);
        else
            slidebox.stop(true).animate({'right':'-430px'},100);
    });
    $('#slidebox .close').on('click',function(){
        $(this).parent().remove();
    }); 
  }
});*/


//Sticky Nav
$(function() {

    var findEl = $('.sh-sticky-wrap').length;
    if (findEl <= 0) {
        // do nothing
    } else {


        //Set the height of the sticky container to the height of the nav
        //var navheight = $('.site-nav-container').height();
        // grab the initial top offset of the navigation 
        var sticky_navigation_offset_top = $('.sh-sticky-wrap').offset().top;
        //var sticky_navigation_offset_top = $('.sh-sticky-wrap').outerHeight();
        // our function that decides weather the navigation bar should have "fixed" css position or not.
        var sticky_navigation = function() {
            var scroll_top = $(window).scrollTop(); // our current vertical position from the top
            // if we've scrolled more than the navigation, change its position to fixed to stick to top,
            // otherwise change it back to relative
            if (scroll_top > sticky_navigation_offset_top) {
                $('.sh-sticky-wrap').addClass('stuck');
                //$('footer').css('padding-bottom',sticky_navigation_offset_top+'px');
                //$('.sh-sticky-inner-wrap').css('height', '187px');
            } else if (scroll_top <= sticky_navigation_offset_top) {
                $('.sh-sticky-wrap').removeClass('stuck');
                //$('footer').css('padding-bottom','0');
                // $('.site-header').css('height', 'auto');
            }
        };
        // run our function on load
        sticky_navigation();
        // and run it again every time you scroll
        $(window).scroll(function() {
            sticky_navigation();
        });

    }
});

//gdd 003 ebook carousel

$(document).ready(function() {
              

              $('.rem-carousel.owl-carousel').owlCarousel({
                loop: true,
                smartSpeed:800,
                margin: 10,
                responsiveClass: true,
                pagination: false,
                autoPlay: 3000,
                autoplayTimeout:1000,
                autoplayHoverPause:true,
                responsive: {
                  0: {
                    items: 1,
                    nav: true
                  },
                  640: {
                    items: 3,
                    nav: true
                  },
                  960: {
                    items: 4,
                    nav: true,
                    loop: false,
                    margin: 0
                  }
                }
              });



});

if (window.vwoElInterval1) {
    clearInterval(window.vwoElInterval1);
}
window.vwoElInterval1 = setInterval(function() {
    if ($('#hsForm_1a616f24-8f3d-485b-9409-86a7b0101905').length) { //Check for element that must exist before rest of code is run.
        var form2 = $('.feature-contact-form .hs-form'); //The form that will be focused
        var page2 = $('body'); //The element (whole page but form) that will be blurred (this can probably be the `body`)
        var formInputs = $('.hs-input'); //form inputs

        $('body').on('click touchstart', function(event) { 
            if( $(event.target).closest('.feature-contact-form .hs-form').length ) { //Check if clicked element is inside a `form` element
                //console.log('Click is inside form!');
                page2.addClass('blur'); 
                $('.feature-contact-form').addClass('form-active');
            } else {
                //console.log('Click is outside form!');
                page2.removeClass('blur'); 
                $('.feature-contact-form').removeClass('form-active');
            }
        });

        formInputs.on( 'focus', function( event ) {
            //This is needed due to `click` event not always triggering on first click in FF & Safari.
            //It is also useful for people using only keyboard shortcuts.
            
            //console.log('Focus is inside form input!');
            page2.addClass('blur'); 
            $('.feature-contact-form').addClass('form-active');
        });
        clearInterval(window.vwoElInterval1);
    }
});

//focus on select input
$(document).ready(function(){
  
$("select").focus(function (event) {
      // alert(123);
  if ($('#hsForm_1a616f24-8f3d-485b-9409-86a7b0101905').length) { //Check for element that must exist before rest of code is run.
        var form2 = $('.feature-contact-form .hs-form'); //The form that will be focused
        var page2 = $('body'); //The element (whole page but form) that will be blurred (this can probably be the `body`)
        var formInputs = $('.hs-input'); //form inputs

        $('body').on('click touchstart', function(event) { 
            if( $(event.target).closest('.feature-contact-form .hs-form').length ) { //Check if clicked element is inside a `form` element
                //console.log('Click is inside form!');
                page2.addClass('blur'); 
                $('.feature-contact-form').addClass('form-active');
            } else {
                //console.log('Click is outside form!');
                page2.removeClass('blur'); 
                $('.feature-contact-form').removeClass('form-active');
            }
        });

        
    }
    
  });

  
 });

$(window).load(function() {
        $(".scs-form .hs-form input[type='text'],textarea").on('change keyup keydown focus blur', function() {
            var input_value = $(this).val().length;
            if (input_value > 0) {
                $(this).parent('.input').addClass('input-has-value');
            } else {
                $(this).parent('.input').removeClass('input-has-value');
            }
        });
        $(".scs-form .hs-form input[type='email']").on('change keyup keydown keypress focus blur', function() {
            var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{3,4}/.test(this.value);
            if (!re) {
                $(this).parent('.input').removeClass('input-has-value');
                $(this).parent('.input').addClass('error');
            } else {
                $(this).parent('.input').addClass('input-has-value');
                $(this).parent('.input').removeClass('error');
            }
        });
        $(".scs-form .hs-form input[type='tel']").on('change keyup keydown keypress focus blur', function() {
            var re1 = /^[0-9+-.]{7,20}$/.test(this.value);
            if (!re1) {
                $(this).parent('.input').removeClass('input-has-value');
                $(this).parent('.input').addClass('error');
            } else {
                $(this).parent('.input').addClass('input-has-value');
                $(this).parent('.input').removeClass('error');
            }
        });
    });


// Accordion Tabs
$(document).ready(function () {
  $('.accordion-tabs').each(function(index) {
    $(this).children('li').first().children('a').addClass('is-active').next().addClass('is-open').show();
  });
  $('.accordion-tabs').on('click', 'li > a.tab-link', function(event) {
    if (!$(this).hasClass('is-active')) {
      event.preventDefault();
      var accordionTabs = $(this).closest('.accordion-tabs');
      accordionTabs.find('.is-open').removeClass('is-open').hide();

      $(this).next().toggleClass('is-open').toggle();
      accordionTabs.find('.is-active').removeClass('is-active');
      $(this).addClass('is-active');
    } else {
      event.preventDefault();
    }
  });
});


$(document).on('ready', function() {
$('.gdd-industries-served-module .tab-link ').on('click', function () {

    var bgimageUrl = "" + $(this).attr('rel') + "";
    $('.gdd-industries-served-module').attr('style', 'background-image: url("' + bgimageUrl +'")');
});



});


/* CUSTOM CODE */
$(document).ready(function(){
    $('.bottom-cta').hide();
  if(!sessionStorage.getItem('gdd_show1')){
    sessionStorage.setItem('gdd_show', 'true'); // set the key in local storage
  }
  if(sessionStorage.getItem('gdd_show')){
      $('.bottom-cta').show(); 
  }
    $('.cta-close').click(function(){
        $('.bottom-cta').remove();
        sessionStorage.removeItem('gdd_show');
        sessionStorage.setItem('gdd_show1', 'true');
      });

  });

$(document).ready(function(){
    $('.side-cta').hide();
  if(!sessionStorage.getItem('gdd_show11')){
    sessionStorage.setItem('gdd_show22', 'true'); // set the key in local storage
  }
  if(sessionStorage.getItem('gdd_show22')){
      $('.side-cta').show(); 
  }
    $('.cta-close1').click(function(){
        $('.side-cta').remove();
        sessionStorage.removeItem('gdd_show22');
        sessionStorage.setItem('gdd_show11', 'true');
      });

  });

$(document).ready(function () {
  /*$(".im-content").hide();
   $(".im-content:first-child").show();

  /* if in tab mode */
    /*$(".imsvg-dot > a").click(function() {
    
      $(".im-content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();    
    
      /*$("ul.im-tabs li").removeClass("active");
      $(this).addClass("active");
      $('.site-intro').children('div').removeClass('active');
      var shownId = $(this).attr('rel');
      $('.site-intro').find('div[data-type="'+shownId+'"]').addClass('active');*/
    
    /*}); */ 
    $(this).find(".im-content-wrap > div:first").addClass('active').show(); //Show first tab content
    var tabs = $('.imsvg-dot > a');
    tabs.bind('click', function(event) {
        var tab_id = $(this).attr('rel');
        //alert(tab_id);
       /* $('.path-svg').attr("class", "path-svg");
        $('g[id = ' + tab_id + ']').attr("class", "has-active path-svg");*/
        var $anchor = $(this);
        var ids = tabs.each(function() {
            $($(this).attr('href')).removeClass('active').hide();
        });
        $($anchor.attr('href')).addClass('active').show();
    });
});

$(document).ready(function() {
  //toggle the component with class accordion_body
  $(".accordion_head").click(function() {
    if ($('.accordion_body').is(':visible')) {
      $(".accordion_body").slideUp(300);
      $(".plusminus").text('+');
    }
    if ($(this).next(".accordion_body").is(':visible')) {
      $(this).next(".accordion_body").slideUp(300);
      $(this).children(".plusminus").text('+');
    } else {
      $(this).next(".accordion_body").slideDown(300);
      $(this).children(".plusminus").text('-');
    }
  });
});


//pillar page
$(document).ready(function() {
  $(function() {
  var findE2 = $('.anchor-links-nav').length; 
  if(findE2 > 0){
  //Set the height of the sticky container to the height of the nav
  //var navheight = $('.site-nav-container').height();
  // grab the initial top offset of the navigation 
  var sticky_navigation_offset_top2 = $('.anchor-links-nav').offset().top - 166;
  var header_height = $('.sh-sticky-wrap').outerHeight();
  var pillar_nav_height = $('.anchor-links-nav').outerHeight();

  // our function that decides weather the navigation bar should have "fixed" css position or not.
  var sticky_navigation2 = function() {
    var scroll_top2 = $(window).scrollTop(); // our current vertical position from the top
    console.log(scroll_top2, sticky_navigation_offset_top2);

    // if we've scrolled more than the navigation, change its position to fixed to stick to top,
    // otherwise change it back to relative
    if (scroll_top2 >= sticky_navigation_offset_top2) {
      $('.anchor-links-nav').addClass('stuck').css('top', header_height+'px');
      $('.additional-content').css('padding-top',pillar_nav_height+'px');
      // $('.sh-sticky-wrap').addClass('stuck').css('height',navheight);
    } else {
      $('.anchor-links-nav').removeClass('stuck').css('top', '0px');
      $('.additional-content').css('padding-top','0');
    }
  };

  // run our function on load
  sticky_navigation2();

  // and run it again every time you scroll
  $(window).scroll(function() {
    sticky_navigation2();
  });
}
});
});

$(document).ready(function() {
  $('.anchor-links-nav a').click(function() {
     $('.anchor-links-nav li').removeClass('active');
     $(this).parent('li').addClass('active');
  });
});

$(document).ready(function() {

    $('.anchor-links-nav a[href*=#]').bind('click', function(e) {
        e.preventDefault(); // prevent hard jump, the default behavior

        var target = $(this).attr("href"); // Set the target as variable
        
        var wWidth = $(window).width();
        if (wWidth >= 1280) {
          // perform animated scrolling by getting top-position of target-element and set it as scroll target
          $('html, body').stop().animate({
              scrollTop: $(target).offset().top  - 166
          }, 600, function() {
              location.hash = target; //attach the hash (#jumptarget) to the pageurl
          });
        }
        

        return false;
    });
});
$(window).scroll(function() {
    var scrollDistance = $(window).scrollTop();
   // console.log($(window).scrollTop());
    // Show/hide menu on scroll
    // if (scrollDistance >= 850) {
    //    $('nav').fadeIn("fast");
    // } else {
    //    $('nav').fadeOut("fast");
    // }
  
    // Assign active class to nav links while scolling
    
      $('.page-inner-anchor').each(function(i) {
         //console.log($(this).position().top);
          if ($(this).position().top - 166 <= scrollDistance) {

              $('.anchor-links-nav li.active').removeClass('active');
              $('.anchor-links-nav li').eq(i).addClass('active');
          }
      });
    
}).scroll();
