<?php
/**
Template Name: Laser Cutting Videos
 */
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
 <?php Starkers_Utilities::get_template_parts( array( 'parts/page-intro' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<!--Site Content-->
<section id="idContentTblCell"><!--PAGE START-->
      <div class="inner-wrap">
 

<div class="video-module rows-of-3">
<?php if( have_rows('vm-add-video') ): while ( have_rows('vm-add-video') ) : the_row(); ?>
  <a class="popup-youtube" href="<?php if(get_sub_field('vm_video_link_or_url')):?><?php the_sub_field('vm_video_link_or_url');?><?php endif;?>"><img src="<?php if(get_sub_field('vm_video_image')):?><?php the_sub_field('vm_video_image');?><?php endif;?>" /><span><?php if(get_sub_field('vm_video_title')):?><?php the_sub_field('vm_video_title');?><?php endif;?></span></a>
  <?php endwhile;
    endif; ?>
</div>
<?php the_content(); ?> 
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
<?php if (is_page( '9' )) : ?>
<!--Sitemap Page-->
   <ul>
   <?php
   // Add pages you'd like to exclude in the exclude here
   wp_list_pages(
   array(
   'exclude' => '',
   'title_li' => '',
   )
   );
   ?>
   </ul>
<?php endif; ?> 


<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar','parts/shared/flexible-content-fullwidth' ) ); ?>
</div>
</section>

<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox' ) ); ?>

<?php
$parent_id = wp_get_post_parent_id( $post_ID );
 if ( ( $parent_id == '951' ) || ($parent_id == '972') ) : 
 Starkers_Utilities::get_template_parts( array( 'parts/shared/portfolio-slider' ) ); 
endif; ?>      
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>